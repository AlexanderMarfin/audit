declare
BCName varchar2(70);
TblName varchar2(70);
ExistFlag number(1);
AppletExists number(1);
BCExistFlag number(1);
begin
	for rec in (SELECT ps.ROW_ID, p.NAME FROM SIEBEL.S_VIEW p INNER JOIN SIEBEL.S_VIEW_WEB_TMPL ps on p.ROW_ID = ps.VIEW_ID WHERE
	p.NAME ='Contact List View' OR p.NAME ='Visible Contact List View' OR p.NAME ='Manager''s Contact List View' OR p.NAME ='All Contacts across Organizations'
	OR p.NAME ='UCM Contact Duplicates View' OR p.NAME ='Consumer List View' OR p.NAME ='Personal Contact List View' OR p.NAME ='FINS Contacts Financial Accounts View' 
	OR p.NAME ='FINCORP Contact Chart View - Lead Status Analysis' OR p.NAME ='Contact Administration View' OR p.NAME ='MDM CIF Contact History List View' 
	OR p.NAME ='MDM Contact Credentials History View' OR p.NAME ='Contact Details View (Detail tab)' OR p.NAME ='MDM Disable Contact Cleansing View' OR p.NAME ='MDM PODFT View' 
	OR p.NAME ='CRS View' OR p.NAME ='FATCA View' OR p.NAME ='MDM Contact Credentials View' OR p.NAME ='Contact Detail - Personal Address View' OR p.NAME ='MDM Contact Phone View' 
	OR p.NAME ='MDM Contact Email View' OR p.NAME ='Relationship Hierarchy View (Contact)' OR p.NAME ='MDM Party Relationship View' OR p.NAME ='MDM Contact Segment View' 
	OR p.NAME ='MDM Contact Black List View' OR p.NAME ='MDM Contact Education View' OR p.NAME ='MDM Contact Employment View' OR p.NAME ='MDM Contact Relativies View' 
	OR p.NAME ='MDM CIF Contact History List Detail View' OR p.NAME ='MDM Contact Credentials History Detail View' OR p.NAME ='UCM Contact Cross Reference View' 
	OR p.NAME ='MDM Contact SDH Detail View' OR p.NAME ='MDM PODFT SDH Detail View' OR p.NAME ='MDM Contact DUL Dossier VBC View' OR p.NAME ='MDM Contact Photo Dossier VBC View' 
	OR p.NAME ='MDM Contact CardSign Dossier VBC View' OR p.NAME ='MDM Contact Service Request View' OR p.NAME ='Contact Detail - Accounts View' OR p.NAME ='Contact Detail View' 
	OR p.NAME ='Contact Activity Plan' OR p.NAME ='Contact Detail - Tasks View' OR p.NAME ='Contact Service Agreement List View' OR p.NAME ='FIN Contact Alerts View' 
	OR p.NAME ='FIN Contact Applications View' OR p.NAME ='Contact Assessment View' OR p.NAME ='Contact Asset Mgmt View' OR p.NAME ='Contact Attachment View' 
	OR p.NAME ='FIN Service Request Acct Services Contact Bill Pay Enrollment View' OR p.NAME ='FIN Service Request Acct Services Contact Bill Payees View' 
	OR p.NAME ='FIN Service Request Acct Services Contact Payment Sched View' OR p.NAME ='FINS Contact Detail - Billing Accounts View' OR p.NAME ='FINS Application Mortgage Calculator - Early Payoff' 
	OR p.NAME ='FINS Application Mortgage Calculator - Loan Amount' OR p.NAME ='FINS Application Mortgage Calculator - Loan Comparison' OR p.NAME ='FINS Application Mortgage Calculator - Monthly Payment View' 
	OR p.NAME ='FINS Application Mortgage Calculator - Refinance Breakeven View' OR p.NAME ='INS Contact Detail - Claims View' OR p.NAME ='FINS Contact Financial Profile View' 
	OR p.NAME ='FINS Investment Contact Profile View' OR p.NAME ='FINS Contact Personal Profile View' OR p.NAME ='FINS Contact Coverage View' OR p.NAME ='Contact Detail - Personal Payment Profile View' 
	OR p.NAME ='FINS Contact Profitability List View' OR p.NAME ='Contact Customer Satisfaction Survey' OR p.NAME ='FINS Contact Disclosures View' OR p.NAME ='Contact Duplicates Detail View' 
	OR p.NAME ='FIN Contact Account View' OR p.NAME ='FINS Financial Planning View' OR p.NAME ='FINS Contact Household View' OR p.NAME ='Contact Invoice View' OR p.NAME ='List Mgmt Contacts Detail View' 
	OR p.NAME ='FINS Contact List Mgmt Lists View' OR p.NAME ='FINS Application Mortgage Pre-Qualification Calculator View' OR p.NAME ='FIN NA RET Checking View' OR p.NAME ='FIN NA RET Convenience View' 
	OR p.NAME ='FIN NA RET Credit View' OR p.NAME ='FINS Investment Portfolio Recommendation View' OR p.NAME ='FINS Application Mortgage NA Pre-Qualification View' OR p.NAME ='FIN NA RET Retirement View' 
	OR p.NAME ='FIN NA RET Saving View' OR p.NAME ='Contact Private Note View' OR p.NAME ='Contact Note View' OR p.NAME ='Contact Offers View' OR p.NAME ='Contact Detail - Opportunities View' 
	OR p.NAME ='INS Contact Detail - Policy View' OR p.NAME ='FINS Application Mortgage NA Product Recom View' OR p.NAME ='FINCORP Contact Profile View' OR p.NAME ='FINCORP Contact Referral View' 
	OR p.NAME ='Contact Relationships View' OR p.NAME ='FIN Contact Service View' OR p.NAME ='INS Contact Detail - Underwriting Report View' OR p.NAME ='MDM Dup Contact Details View' 
	OR p.NAME ='MDM Dup Contact Credentials View' OR p.NAME ='MDM Dup Contact Detail - Personal Address View' OR p.NAME ='MDM Dup Contact Phone View' OR p.NAME ='MDM Dup Contact Email View' 
	OR p.NAME ='MDM Dup Contact Segment View' OR p.NAME ='MDM Dup Contact Black List View' OR p.NAME ='MDM Dup Contact Education View' OR p.NAME ='MDM Dup Contact Employment View' 
	OR p.NAME ='MDM Dup Contact Relativies View' OR p.NAME ='MDM Dup CIF Contact Reference View' OR p.NAME ='MDM Contact UnMerge Detail View' OR p.NAME ='MDM Contact Credentials UnMerge View' 
	OR p.NAME ='MDM Contact Address UnMerge View' OR p.NAME ='MDM Contact Phone UnMerge View' OR p.NAME ='MDM Contact Email UnMerge View' OR p.NAME ='MDM Contact Segment UnMerge View' 
	OR p.NAME ='MDM Contact Black List UnMerge View' OR p.NAME ='MDM Contact Education UnMerge View' OR p.NAME ='MDM Contact Employment UnMerge View' OR p.NAME ='MDM Contact Relativies UnMerge View' 
	OR p.NAME ='Consumer Detail View' OR p.NAME ='Consumer Asset Mgmt - Asset View' OR p.NAME ='Consumer Service Request View')
	loop
		begin
		for rec1 in (SELECT APPLET_NAME FROM SIEBEL.S_VIEW_WTMPL_IT WHERE VIEW_WEB_TMPL_ID = rec.ROW_ID) 
		loop		
			begin
			SELECT CASE WHEN (EXISTS(SELECT APPLET_NAME FROM SIEBEL.S_VIEW_WTMPL_IT WHERE APPLET_NAME = rec1.APPLET_NAME AND INACTIVE_FLG!='Y') AND
			EXISTS(SELECT BUSCOMP_NAME FROM SIEBEL.S_APPLET WHERE NAME = rec1.APPLET_NAME AND INACTIVE_FLG!='Y'))
			THEN 1
			ELSE 0
			END into AppletExists from dual; 
			if AppletExists=1 
			then
			SELECT DISTINCT BUSCOMP_NAME into BCName FROM SIEBEL.S_APPLET WHERE NAME = rec1.APPLET_NAME;
			SELECT DISTINCT TABLE_NAME into TblName FROM SIEBEL.S_BUSCOMP WHERE NAME = BCName;
			begin
				select case
				When exists(SELECT BUSCOMP_NAME FROM SIEBEL.S_AUDIT_BUSCOMP WHERE BUSCOMP_NAME = BCName) 
				then 1 
				else 0 
				end into ExistFlag 
				from dual;
				end;
					if ExistFlag = 0 and TblName is not null
					then
						INSERT INTO SIEBEL.S_AUDIT_BUSCOMP (ROW_ID,CONFLICT_ID, CREATED_BY, LAST_UPD_BY, DB_LAST_UPD, DB_LAST_UPD_SRC, ASSOC_FLG, BASE_TBL_NAME, 
						BUSCOMP_NAME, COPY_FLG, DELETE_FLG, EXPORT_FLG, NEW_FLG, RESTRICTION_CD, START_DT, SYS_AUDIT_FLG, UPDATE_FLG, END_DT, DESC_TEXT) 
						VALUES (siebel.s_sequence_pkg.get_next_rowid, '0', 'ScriptAuditMDM', '0-1', sysdate-1, 'ScriptAuditMDM','N',TblName, 
						BCName,'N','N','Y','N','Без ограничений',sysdate-1,'N','N',null,null);
						commit;
						INSERT INTO SIEBEL.CX_BC_AUDIT (ROW_ID,CONFLICT_ID, CREATED_BY, LAST_UPD_BY, DB_LAST_UPD, DB_LAST_UPD_SRC, 
						BUSCOMP, BUSCOMP_NUMBER)
						VALUES (siebel.s_sequence_pkg.get_next_rowid, '0', 'ScriptAuditMDM', '0-1', sysdate-1, 'ScriptAuditMDM', 
						BCName,1);
						commit;
					end if;
					if ExistFlag = 1
					then
						UPDATE SIEBEL.S_AUDIT_BUSCOMP SET EXPORT_FLG = 'Y' WHERE BUSCOMP_NAME=BCName; 
						commit;
						UPDATE SIEBEL.S_AUDIT_BUSCOMP SET END_DT = null WHERE BUSCOMP_NAME=BCName; 
						commit; 
					begin 
						select case 
						when exists(SELECT BUSCOMP FROM SIEBEL.CX_BC_AUDIT WHERE BUSCOMP = BCName) 
						then 1 
						else 0 
						end into BCExistFlag 
						from dual;
					end;
					if BCExistFlag = 0
					then
						INSERT INTO SIEBEL.CX_BC_AUDIT (ROW_ID, CONFLICT_ID, CREATED_BY, LAST_UPD_BY, DB_LAST_UPD, DB_LAST_UPD_SRC, 
						BUSCOMP, BUSCOMP_NUMBER)
						VALUES (siebel.s_sequence_pkg.get_next_rowid, '0', 'ScriptAuditMDM', '0-1', sysdate-1, 'ScriptAuditMDM', 
						BCName,1);
						commit; 
					else
						UPDATE SIEBEL.CX_BC_AUDIT SET BUSCOMP_NUMBER = BUSCOMP_NUMBER+1 WHERE BUSCOMP=BCName; 
						commit;
					end if;
					end if;
					end if; 
			end; 
		end loop;
		end;
	end loop;
	INSERT INTO SIEBEL.S_AUDIT_BUSCOMP (ROW_ID,CONFLICT_ID, CREATED_BY, LAST_UPD_BY, DB_LAST_UPD, DB_LAST_UPD_SRC, ASSOC_FLG, BASE_TBL_NAME, 
	BUSCOMP_NAME, COPY_FLG, DELETE_FLG, EXPORT_FLG, NEW_FLG, RESTRICTION_CD, START_DT, SYS_AUDIT_FLG, UPDATE_FLG, END_DT, DESC_TEXT) 
	VALUES (siebel.s_sequence_pkg.get_next_rowid, '0', 'ScriptAuditMDM', '0-1', sysdate-1, 'ScriptAuditMDM','N','S_PARTY', 
	'MDM Merged Contact','N','N','Y','N','Без ограничений',sysdate-1,'N','N',null,null);
	commit;
	INSERT INTO SIEBEL.CX_BC_AUDIT (ROW_ID, CONFLICT_ID, CREATED_BY, LAST_UPD_BY, DB_LAST_UPD, DB_LAST_UPD_SRC, 
	BUSCOMP, BUSCOMP_NUMBER)
	VALUES (siebel.s_sequence_pkg.get_next_rowid, '0', 'ScriptAuditMDM', '0-1', sysdate-1, 'ScriptAuditMDM',
	'MDM Merged Contact',1);
	commit;
end;
/
